#!/usr/bin/env python3


import feedparser


def getLast10CovidNews():
    feed = "https://koronavirus.gov.hu/cikkek/rss.xml"
    d = feedparser.parse(feed)
    return d.entries[0:10]


def printLast10CovidNews():
    newsentries = getLast10CovidNews()
    for entry in newsentries:
        print("\n".join([entry.title, entry.link, entry.published]),"\n")


def createEntryList():
    newsentries = getLast10CovidNews()
    entryList = []
    for entry in newsentries:
        entryList.append((entry.title, entry.link, entry.published))
    return entryList

