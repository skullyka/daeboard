import os
import urllib


class CsvGetter:
    def __init__(self, csv_file):
        self.csv_source = "https://covid.observer/hu/hu-covid.observer.csv"
        self.csv_file = csv_file
        self.getCsvFile()

    def getCsvFile(self):
        self.downloadStatsCsv()
        self.changeCsvTableHeader()

    def downloadStatsCsv(self):
        urllib.request.urlretrieve(self.csv_source, self.csv_file)


    def changeCsvTableHeader(self):
        source_fp = open(self.csv_file, 'r')
        target_fp = open(self.csv_file + ".new", 'w')
        first_row = True
        for row in source_fp:
            if first_row:
                row = self.reformatHeader(row)
                first_row = False
            target_fp.write(row)
        os.remove(self.csv_file)
        os.rename(self.csv_file + ".new", self.csv_file)

    def reformatHeader(self, row):
        filtered_row = row.replace(", %", "")
        new_col_names = filtered_row.replace(" ", "_")
        return new_col_names


