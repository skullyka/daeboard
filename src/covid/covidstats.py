import matplotlib.pyplot as plt


class CovidPlotGenerator:
    def __init__(self, dh):
        self.datahandler = dh

    def createDailyPlot(self, png_path):
        png = png_path
        self.drawPlot(self.datahandler.activeCases(), self.datahandler.newCases(), png)

    def create7dayPlot(self, png_path):
        average_7days_ac = self.generateNDaySegments(self.datahandler.activeCases(), 7)
        average_7days_nc = self.generateNDaySegments(self.datahandler.newCases(), 7)
        png = png_path
        self.drawPlot(average_7days_ac, average_7days_nc, png)

    def drawPlot(self, xaxis, yaxis, filename):
        plt.figure(figsize=(10, 10))
        self.plotProgress(xaxis, yaxis)
        self.plotLastPoint(xaxis, yaxis)
        plt.legend()
        plt.xlim(0)
        plt.ylim(0)
        plt.grid(True)
        self.savePlot(filename)

    def savePlot(self, filename):
        plt.savefig(filename)

    def plotLastPoint(self, xaxis, yaxis):
        plt.plot(xaxis[0], yaxis[0], 'rx', label="Now")

    def plotProgress(self, xaxis, yaxis):
        plt.plot(xaxis, yaxis, '-x', label="The progress")

    def generateNDaySegments(self, cases, days):
        segmented_cases = [sum(cases[i:i + days]) / 7 for i in range(0, len(cases), days)]
        return segmented_cases
