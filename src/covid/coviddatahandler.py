import pandas as pd

from covid import covidcsvgetter


class CovidDataHandler:
    def __init__(self, file_path):
        self.file_path = file_path
        covidcsvgetter.CsvGetter(self.file_path)
        self.df = pd.read_csv(self.file_path)
        self.addNewCasesColumn()

    def addNewCasesColumn(self):
        new_cases = self.createNewCasesColumn()
        self.df["New_cases"] = new_cases

    def createNewCasesColumn(self):
        new_cases = []
        for i in range(self.df.Date.count() - 1):
            new_cases.append(self.df.T[i].Confirmed_cases - self.df.T[i + 1].Confirmed_cases)
        new_cases.append(0)
        return new_cases

    def activeCases(self):
        return self.df.Active_cases

    def newCases(self):
        return self.df.New_cases