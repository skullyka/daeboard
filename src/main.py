#!/usr/bin/env python3

from covid import covidnews
from covid import covidstats
from covid import coviddatahandler
from dae import createdae


def main():
    covidnews.printLast10CovidNews()
    dh = coviddatahandler.CovidDataHandler("./data/covid_hu.csv")
    plotgenerator = covidstats.CovidPlotGenerator(dh)
    png_path = 'data/active_new_cases_7days.png'
    plotgenerator.create7dayPlot(png_path)
    png_path = 'data/active_new_cases.png'
    plotgenerator.createDailyPlot(png_path)
    rawentries = []
    asd = createdae.DaeboardCreator(rawentries)


if __name__ == "__main__":
    main()
